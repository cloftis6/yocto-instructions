# Setup for Yocto build


## Steps to build Yocto image
See setup.sh
1. Clone main repo: `git clone -b dunfell git://git.yoctoproject.org/poky`
2. Clone additional layers to the poky folder
`git clone -b dunfell git://git.repo/meta-name`
3. Install build prerequsites
See Yocto documentation for build requirements. Requires specifi Linux distro etc. Or use a docker container from the poky directory for a self contained build environment. See https://wiki.yoctoproject.org/wiki/TipsAndTricks/CropsCLIContainers
`docker run --rm -it -v $(pwd):$(pwd) crops/poky:opensuse-15.1 --workdir=$(pwd)`
4. Start yocto build environment
`source oe-init-build-env project-name`
5. Setup the build - add layers and local.conf
`bitbake-layers add-layer ../meta-layer-name/`
`Edit conf/local.conf`
6. Build image - Going to take a few hours the first run
`bitbake core-image-base`
Image gets created in: ./tmp/deploy/images/machine/core-image-base-machine.wic Replace `machine` with the name of the current target.
7. Copy image to sdcard
`dd if=tmp/deploy/images/machine/core-image-base-machine.wic of=/dev/sdcard/device`
8. Build RAUC update package
`bitbake update-bundle`


## Notes
* Remove the tmp directory when making any change to the build. Bitbake is terrible about detecting changes so it's easier to just have it regenerate it. Doing this doesn't make the build take that much longer. Most of the time consuming stuff is stored in cache.
* When coming back with a new terminal, have to run the docker container and source oe-init-build-env again. This sets up the bitbake command.
