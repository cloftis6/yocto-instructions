#!/bin/bash


# Download main Yocto repo
git clone -b dunfell git://git.yoctoproject.org/poky

# Download additional layers
cd poky
git clone -b dunfell git://git.openembedded.org/meta-openembedded
git clone -b dunfell git://git.yoctoproject.org/meta-raspberrypi
git clone -b dunfell https://github.com/rauc/meta-rauc.git
git clone -b dunfell https://user@bitbucket.org/cloftis6/meta-rauc-community.git
git clone -b dunfell https://user@bitbucket.org/cloftis6/meta-bbbw.git
git clone -b dunfell https://user@bitbucket.org/cloftis6/meta-wifi-credentials.git

# Setup docker container containing Yocto build environment
docker run --rm -it -v $(pwd):$(pwd) crops/poky:opensuse-15.1 --workdir=$(pwd)


# Create raspberry pi build
source oe-init-build-env build-rpi
bitbake-layers add-layer ../meta-openembedded/meta-oe/
bitbake-layers add-layer ../meta-openembedded/meta-python/
bitbake-layers add-layer ../meta-openembedded/meta-networking/
bitbake-layers add-layer ../meta-openembedded/meta-multimedia/
bitbake-layers add-layer ../meta-raspberrypi/
bitbake-layers add-layer ../meta-rauc
bitbake-layers add-layer ../meta-rauc-community/meta-rauc-raspberrypi/
bitbake-layers add-layer ../meta-wifi-credentials
cat ../../local-rpi.conf >> ./conf/local.conf

cd ..

# Create beaglebone black /w build
source oe-init-build-env build-bbb
bitbake-layers add-layer ../meta-openembedded/meta-oe/
bitbake-layers add-layer ../meta-openembedded/meta-python/
bitbake-layers add-layer ../meta-openembedded/meta-networking/
bitbake-layers add-layer ../meta-openembedded/meta-multimedia/
bitbake-layers add-layer ../meta-bbbw/
bitbake-layers add-layer ../meta-rauc
bitbake-layers add-layer ../meta-rauc-community/meta-rauc-beaglebone/
bitbake-layers add-layer ../meta-wifi-credentials
cat ../../local-bbb.conf >> ./conf/local.conf


